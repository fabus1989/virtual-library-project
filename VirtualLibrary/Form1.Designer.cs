﻿namespace VirtualLibrary
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.orderLbl = new System.Windows.Forms.Label();
            this.cartBtn = new System.Windows.Forms.Button();
            this.completeOrderBtn = new System.Windows.Forms.Button();
            this.orderAndBooksLbl = new System.Windows.Forms.Label();
            this.ordersRadioBtn = new System.Windows.Forms.RadioButton();
            this.booksRadioBtn = new System.Windows.Forms.RadioButton();
            this.booksLayoutPnl = new System.Windows.Forms.FlowLayoutPanel();
            this.mailLbl = new System.Windows.Forms.Label();
            this.mailTextBox = new System.Windows.Forms.TextBox();
            this.allBtn = new System.Windows.Forms.Button();
            this.infoTxtBx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // orderLbl
            // 
            this.orderLbl.AutoSize = true;
            this.orderLbl.Location = new System.Drawing.Point(12, 72);
            this.orderLbl.Name = "orderLbl";
            this.orderLbl.Size = new System.Drawing.Size(145, 13);
            this.orderLbl.TabIndex = 0;
            this.orderLbl.Text = "Available Books / Your Order";
            // 
            // cartBtn
            // 
            this.cartBtn.Location = new System.Drawing.Point(952, 101);
            this.cartBtn.Name = "cartBtn";
            this.cartBtn.Size = new System.Drawing.Size(206, 23);
            this.cartBtn.TabIndex = 2;
            this.cartBtn.Text = "Add Book(s) To Cart";
            this.cartBtn.UseVisualStyleBackColor = true;
            this.cartBtn.Click += new System.EventHandler(this.chartBtn_Click);
            // 
            // completeOrderBtn
            // 
            this.completeOrderBtn.Location = new System.Drawing.Point(953, 142);
            this.completeOrderBtn.Name = "completeOrderBtn";
            this.completeOrderBtn.Size = new System.Drawing.Size(205, 23);
            this.completeOrderBtn.TabIndex = 3;
            this.completeOrderBtn.Text = "Complete Your Order";
            this.completeOrderBtn.UseVisualStyleBackColor = true;
            this.completeOrderBtn.Click += new System.EventHandler(this.completeOrderBtn_Click);
            // 
            // orderAndBooksLbl
            // 
            this.orderAndBooksLbl.AutoSize = true;
            this.orderAndBooksLbl.Location = new System.Drawing.Point(13, 9);
            this.orderAndBooksLbl.Name = "orderAndBooksLbl";
            this.orderAndBooksLbl.Size = new System.Drawing.Size(128, 13);
            this.orderAndBooksLbl.TabIndex = 5;
            this.orderAndBooksLbl.Text = "Check books / your order";
            // 
            // ordersRadioBtn
            // 
            this.ordersRadioBtn.AutoSize = true;
            this.ordersRadioBtn.Location = new System.Drawing.Point(16, 41);
            this.ordersRadioBtn.Name = "ordersRadioBtn";
            this.ordersRadioBtn.Size = new System.Drawing.Size(121, 17);
            this.ordersRadioBtn.TabIndex = 10;
            this.ordersRadioBtn.Text = "Show Orders History";
            this.ordersRadioBtn.UseVisualStyleBackColor = true;
            this.ordersRadioBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ordersRadioBtn_MouseClick);
            // 
            // booksRadioBtn
            // 
            this.booksRadioBtn.AutoSize = true;
            this.booksRadioBtn.Location = new System.Drawing.Point(143, 42);
            this.booksRadioBtn.Name = "booksRadioBtn";
            this.booksRadioBtn.Size = new System.Drawing.Size(131, 17);
            this.booksRadioBtn.TabIndex = 11;
            this.booksRadioBtn.Text = "Show Available Books";
            this.booksRadioBtn.UseVisualStyleBackColor = true;
            this.booksRadioBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.booksRadioBtn_MouseClick);
            // 
            // booksLayoutPnl
            // 
            this.booksLayoutPnl.AutoScroll = true;
            this.booksLayoutPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.booksLayoutPnl.Location = new System.Drawing.Point(16, 101);
            this.booksLayoutPnl.Name = "booksLayoutPnl";
            this.booksLayoutPnl.Size = new System.Drawing.Size(930, 359);
            this.booksLayoutPnl.TabIndex = 12;
            // 
            // mailLbl
            // 
            this.mailLbl.AutoSize = true;
            this.mailLbl.Location = new System.Drawing.Point(670, 9);
            this.mailLbl.Name = "mailLbl";
            this.mailLbl.Size = new System.Drawing.Size(115, 13);
            this.mailLbl.TabIndex = 13;
            this.mailLbl.Text = "Please type your email:";
            // 
            // mailTextBox
            // 
            this.mailTextBox.Location = new System.Drawing.Point(673, 42);
            this.mailTextBox.Name = "mailTextBox";
            this.mailTextBox.Size = new System.Drawing.Size(273, 20);
            this.mailTextBox.TabIndex = 14;
            // 
            // allBtn
            // 
            this.allBtn.Location = new System.Drawing.Point(953, 185);
            this.allBtn.Name = "allBtn";
            this.allBtn.Size = new System.Drawing.Size(205, 23);
            this.allBtn.TabIndex = 15;
            this.allBtn.Text = "Select All";
            this.allBtn.UseVisualStyleBackColor = true;
            this.allBtn.Click += new System.EventHandler(this.allBtn_Click);
            // 
            // infoTxtBx
            // 
            this.infoTxtBx.Enabled = false;
            this.infoTxtBx.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.infoTxtBx.Location = new System.Drawing.Point(953, 225);
            this.infoTxtBx.Multiline = true;
            this.infoTxtBx.Name = "infoTxtBx";
            this.infoTxtBx.Size = new System.Drawing.Size(205, 235);
            this.infoTxtBx.TabIndex = 16;
            this.infoTxtBx.Text = "Welcome to Library! Please choose your books!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1170, 473);
            this.Controls.Add(this.infoTxtBx);
            this.Controls.Add(this.allBtn);
            this.Controls.Add(this.mailTextBox);
            this.Controls.Add(this.mailLbl);
            this.Controls.Add(this.booksLayoutPnl);
            this.Controls.Add(this.booksRadioBtn);
            this.Controls.Add(this.ordersRadioBtn);
            this.Controls.Add(this.orderAndBooksLbl);
            this.Controls.Add(this.completeOrderBtn);
            this.Controls.Add(this.cartBtn);
            this.Controls.Add(this.orderLbl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Virtual Library .NET";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label orderLbl;
        private System.Windows.Forms.Button cartBtn;
        private System.Windows.Forms.Button completeOrderBtn;
        private System.Windows.Forms.Label orderAndBooksLbl;
        private System.Windows.Forms.RadioButton ordersRadioBtn;
        private System.Windows.Forms.RadioButton booksRadioBtn;
        private System.Windows.Forms.FlowLayoutPanel booksLayoutPnl;
        private System.Windows.Forms.Label mailLbl;
        private System.Windows.Forms.TextBox mailTextBox;
        private System.Windows.Forms.Button allBtn;
        private System.Windows.Forms.TextBox infoTxtBx;
    }
}

