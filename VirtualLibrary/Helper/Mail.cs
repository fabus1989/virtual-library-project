﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

namespace VirtualLibrary.Helper
{
    class Mail
    {
        public static void SendEmailWhenComplete(string MailAddress, Order order)
        {
            try
            {
                MailMessage email = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                email.From = new MailAddress("TLAPPLICATIONV2.0@gmail.com");
                email.To.Add(MailAddress);
                email.Subject = "Your Order is complete!";

                email.IsBodyHtml = true;
                email.Body = "Hi,<br><br> Check details of your order. <br>" + "<br>" 
                    + "Books Amount: " + order.OrderedBooks.Count + "<br>" + " Creation Date: "
                    + order.CreationTime.ToShortDateString() + "<br>" + "Expiration Date: "
                    + order.ExpirationTime.ToShortDateString() + "<br>" + "Titles: " + String.Join(", ", order.OrderedBooks);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("TLAPPLICATIONV2.0@gmail.com", "TaskList123!");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(email);
                MessageBox.Show("Order details have been sent on your email!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
