﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VirtualLibrary.Models; // models namespace
using System.IO;

namespace VirtualLibrary.Helper
{
    public static class Configuration //class which serializes and deserializes models and works with DB xml file
    {
        const string FILEPATH = @"Data.xml";
        static XmlSerializer serializer = new XmlSerializer(typeof(Data));

        public static void SerializeModel(Data data)
        {
            using(StreamWriter sw = new StreamWriter(FILEPATH)) 
            {
                serializer.Serialize(sw, data);
            }
        }

        public static Data DeserializeModel()
        {
            using (StreamReader sr = new StreamReader(FILEPATH))
            {
                if (File.Exists(FILEPATH))
                {
                    return (Data)serializer.Deserialize(sr);
                }
            }
            return null;
        }
    }
}
