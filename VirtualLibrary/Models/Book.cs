﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace VirtualLibrary
{
    public class Book
    {
        private string author, title;
        public string Title { get { return this.title; } set { this.title = value; } }
        public string Author { get { return this.author; } set { this.author = value; } }
    }
}
