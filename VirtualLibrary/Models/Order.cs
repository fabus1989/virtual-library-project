﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Mail;

namespace VirtualLibrary
{
    public class Order
    {
        public List<String> OrderedBooks { get; set; }
        public DateTime CreationTime { get; set; } 
        public DateTime ExpirationTime { get; set; }

        public Order()
        {
            OrderedBooks = new List<string>();
        }
      }
    }
