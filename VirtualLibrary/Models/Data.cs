﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualLibrary.Models
{
    public class Data
    {
        public List<Book> Books { get; set; }
        public List<Order> Orders { get; set; }

        public Data()
        {
            Books = new List<Book>();
            Orders = new List<Order>();
        }
    }
}
