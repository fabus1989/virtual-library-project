﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Mail;
using VirtualLibrary.Models; // namespace to access models
using VirtualLibrary.Helper; // namespace to access helpers

namespace VirtualLibrary
{

    public partial class Form1 : Form
    {
        Order Order;
        Data dt;

        public Form1()
        {
            InitializeComponent();
            dt = new Data();
            Order = new Order(); // Dopisz po co tworzysz ten obiekt lub zmien nazwe na mówiącą co bedzie przechowywał, np. currentOrder albo tmpOrder
            dt = Configuration.DeserializeModel();
        }

        private void chartBtn_Click(object sender, EventArgs e) // add to cart
        {
            foreach (var item in booksLayoutPnl.Controls)
            {
                if (((CheckBox)item).Checked) // podwójne rzutowanie, ja bym przed zrzutował to na nowy obiekt CheckBox
                    Order.OrderedBooks.Add(((CheckBox)item).Text); // podwójne rzutowanie
            }
            Order.CreationTime = DateTime.Now;
            Order.ExpirationTime = Order.CreationTime.AddDays(7);
            dt.Orders.Add(Order);
            infoTxtBx.Text = "Please enter your email and complete order!";
        }

        private void completeOrderBtn_Click(object sender, EventArgs e) // complete order and send email
        {
            if (mailTextBox.Text != "")
            {
                Mail.SendEmailWhenComplete(mailTextBox.Text, Order);
                Configuration.SerializeModel(dt);       
                infoTxtBx.Text = "Welcome to Library! Please choose your books!";
            }
            else
            {
                MessageBox.Show("Please type your email!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (booksRadioBtn.Checked)
                {
                    infoTxtBx.Text = "Available books...\r\nPlease choose from list and book(s) to cart."; // uzywanie \r\n jako nowej linii nie jest wpełni bezpieczne, lepiej wkleić tam Environment.NewLine
                }
                else
                {
                    infoTxtBx.Text = "Welcome to Library! Please choose your books!";
                }
            }           
        }

        private void booksRadioBtn_MouseClick(object sender, MouseEventArgs e) // show books
        {
            // zgrupuj kontrolki (GroupBox) i wtedy jednym poleceniem ukrywasz/pokazujesz całą grupę
        
            removeControlsFromPanel();
            AddBooksToForm();
            booksRadioBtn.Enabled = false;
            ordersRadioBtn.Enabled = true;
            cartBtn.Enabled = true;
            allBtn.Enabled = true;
            completeOrderBtn.Enabled = true;
            infoTxtBx.Text = "Available books...\r\nPlease choose from list and book(s) to cart."; // j.w. \r\n
        }

        private void ordersRadioBtn_MouseClick(object sender, MouseEventArgs e) // show orders
        {
            // zgrupuj kontrolki (GroupBox) i wtedy jednym poleceniem ukrywasz/pokazujesz całą grupę
        
            removeControlsFromPanel();
            AddOrdersToForm();
            booksRadioBtn.Enabled = true;
            ordersRadioBtn.Enabled = false;
            cartBtn.Enabled = false;
            completeOrderBtn.Enabled = false;
            allBtn.Enabled = false;
            infoTxtBx.Text = "Orders history...";
        }

        private void removeControlsFromPanel()
        {
            //List<Control> listControls = new List<Control>();

            //foreach (Control control in booksLayoutPnl.Controls)
            //{
                //listControls.Add(control);
            //}

            //foreach (Control control in listControls)
            //{
                //booksLayoutPnl.Controls.Remove(control);
                //control.Dispose();
            //}
            
            // zamiast powyższego
            booksLayoutPnl.Controls.Clear();
        }

        private void AddBooksToForm()
        {
            // dt = Configuration.DeserializeModel(); not needed, model can be deserialized only once - on app start
            foreach (var item in dt.Books)
            {
                CheckBox cb = new CheckBox();
                booksLayoutPnl.Controls.Add(cb);
                cb.Width = 400;
                cb.Text = item.Author + " -- " + item.Title;
            }
        }

        private void AddOrdersToForm()
        {
            // dt = Configuration.DeserializeModel(); not needed, model can be deserialized only once - on app start
            foreach (var item in dt.Orders)
            {
                CheckBox cb = new CheckBox();
                booksLayoutPnl.Controls.Add(cb);
                cb.Width = 2000;
                cb.Text = "Ordered Books: " + String.Join(", ", item.OrderedBooks) + ", Expiration Date: " + item.ExpirationTime.ToShortDateString();
            }
        }

        private void allBtn_Click(object sender, EventArgs e)
        {
            foreach (var item in booksLayoutPnl.Controls)
            {
                if(item is CheckBox)
                {
                    (((CheckBox)item).Checked) = true;
                    infoTxtBx.Text = "Books selected! \r\nPlease add them to your cart!"; // j.w. \r\n
                }            
            }
        }
    }
}
